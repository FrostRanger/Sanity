--[[
sanity
This mod adds sanity to players, a special attribute

License: MIT License
]]

--[===[
  Initialization
]===]

local S = minetest.get_translator("sanity")

sanity = {}
sanity.playerlist = {}

sanity.settings = {}
sanity.settings.default_max = 100
sanity.settings.default_regen = 0
sanity.settings.regen_timer = 0

do
  local default_max = tonumber(minetest.settings:get("sanity_default_max"))
  if default_max ~= nil then
    sanity.settings.default_max = default_max
  end

  local default_regen = tonumber(minetest.settings:get("sanity_default_regen"))
  if default_regen ~= nil then
    sanity.settings.default_regen = default_regen
  end

  local regen_timer = tonumber(minetest.settings:get("sanity_regen_timer"))
  if regen_timer ~= nil then
    sanity.settings.regen_timer = regen_timer
  end
end


--[===[
  API functions
]===]

function sanity.set(playername, value)
  if value < 0 then
    minetest.log("info", "[sanity] Warning: sanity.set was called with negative value!")
    value = 0
  end
  value = sanity.round(value)
  if value > sanity.playerlist[playername].maxsanity then
    value = sanity.playerlist[playername].maxsanity
  end
  if sanity.playerlist[playername].sanity ~= value then
    sanity.playerlist[playername].sanity = value
    sanity.hud_update(playername)
  end
end

function sanity.setmax(playername, value)
  if value < 0 then
    value = 0
    minetest.log("info", "[sanity] Warning: sanity.setmax was called with negative value!")
  end
  value = sanity.round(value)
  if sanity.playerlist[playername].maxsanity ~= value then
    sanity.playerlist[playername].maxsanity = value
    if(sanity.playerlist[playername].sanity > value) then
      sanity.playerlist[playername].sanity = value
    end
    sanity.hud_update(playername)
  end
end

function sanity.setregen(playername, value)
  sanity.playerlist[playername].regen = value
end

function sanity.get(playername)
  return sanity.playerlist[playername].sanity
end

function sanity.getmax(playername)
  return sanity.playerlist[playername].maxsanity
end

function sanity.getregen(playername)
  return sanity.playerlist[playername].regen
end

function sanity.add_up_to(playername, value)
  local t = sanity.playerlist[playername]
  value = sanity.round(value)
  if(t ~= nil and value >= 0) then
    local excess
    if((t.sanity + value) > t.maxsanity) then
      excess = (t.sanity + value) - t.maxsanity
      t.sanity = t.maxsanity
    else
      excess = 0
      t.sanity = t.sanity + value
    end
    sanity.hud_update(playername)
    return true, excess
  else
    return false
  end
end

function sanity.add(playername, value)
  local t = sanity.playerlist[playername]
  value = sanity.round(value)
  if(t ~= nil and ((t.sanity + value) <= t.maxsanity) and value >= 0) then
    t.sanity = t.sanity + value
    sanity.hud_update(playername)
    return true
  else
    return false
  end
end

function sanity.subtract(playername, value)
  local t = sanity.playerlist[playername]
  value = sanity.round(value)
  if(t ~= nil and t.sanity >= value and value >= 0) then
    t.sanity = t.sanity -value
    sanity.hud_update(playername)
    return true
  else
    return false
  end
end

function sanity.subtract_up_to(playername, value)
  local t = sanity.playerlist[playername]
  value = sanity.round(value)
  if(t ~= nil and value >= 0) then
    local missing
    if((t.sanity - value) < 0) then
      missing = math.abs(t.sanity - value)
      t.sanity = 0
    else
      missing = 0
      t.sanity = t.sanity - value
    end
    sanity.hud_update(playername)
    return true, missing
  else
    return false
  end
end





--[===[
  File handling, loading data, saving data, setting up stuff for players.
]===]


-- Load the playerlist from a previous session, if available.
do
  local filepath = minetest.get_worldpath().."/sanity.mt"
  local file = io.open(filepath, "r")
  if file then
    minetest.log("action", "[sanity] sanity.mt opened.")
    local string = file:read()
    io.close(file)
    if(string ~= nil) then
      local savetable = minetest.deserialize(string)
      sanity.playerlist = savetable.playerlist
      minetest.debug("[sanity] sanity.mt successfully read.")
    end
  end
end

function sanity.save_to_file()
  local savetable = {}
  savetable.playerlist = sanity.playerlist

  local savestring = minetest.serialize(savetable)

  local filepath = minetest.get_worldpath().."/sanity.mt"
  local file = io.open(filepath, "w")
  if file then
    file:write(savestring)
    io.close(file)
    minetest.log("action", "[sanity] Wrote sanity data into "..filepath..".")
  else
    minetest.log("error", "[sanity] Failed to write sanity data into "..filepath..".")
  end
end


minetest.register_on_respawnplayer(function(player)
  local playername = player:get_player_name()
  sanity.set(playername, 40)
  sanity.hud_update(playername)
end)


minetest.register_on_leaveplayer(function(player)
  local playername = player:get_player_name()
  if not minetest.get_modpath("hudbars") ~= nil then
    sanity.hud_remove(playername)
  end
  sanity.save_to_file()
end)

minetest.register_on_shutdown(function()
  minetest.log("action", "[sanity] Server shuts down. Rescuing data into sanity.mt")
  sanity.save_to_file()
end)

minetest.register_on_joinplayer(function(player)
  local playername = player:get_player_name()

  if sanity.playerlist[playername] == nil then
    sanity.playerlist[playername] = {}
    sanity.playerlist[playername].sanity = 75
    sanity.playerlist[playername].maxsanity = sanity.settings.default_max
    sanity.playerlist[playername].regen = sanity.settings.default_regen
    sanity.playerlist[playername].remainder = 0
  end

  if minetest.get_modpath("hudbars") ~= nil then
    hb.init_hudbar(player, "sanity", sanity.get(playername), sanity.getmax(playername))
  else
    sanity.hud_add(playername)
  end
end)


--[===[
  sanity regeneration
]===]

sanity.regen_timer = 0

minetest.register_globalstep(function(dtime)
  sanity.regen_timer = sanity.regen_timer + dtime
  if sanity.regen_timer >= sanity.settings.regen_timer then
    local factor = math.floor(sanity.regen_timer / sanity.settings.regen_timer)
    local players = minetest.get_connected_players()
    for i=1, #players do
      local name = players[i]:get_player_name()
      if sanity.playerlist[name] ~= nil then
        if players[i]:get_hp() > 0 then
          local plus = sanity.playerlist[name].regen * factor
          -- Compability check for version <= 1.0.2 which did not have the remainder field
          if sanity.playerlist[name].remainder ~= nil then
            plus = plus + sanity.playerlist[name].remainder
          end
          local plus_now = math.floor(plus)
          local floor = plus - plus_now
          if plus_now > 0 then
            sanity.add_up_to(name, plus_now)
          else
            sanity.subtract_up_to(name, math.abs(plus_now))
          end
          sanity.playerlist[name].remainder = floor
        end
      end
    end
    sanity.regen_timer = sanity.regen_timer % sanity.settings.regen_timer
  end
end)

--[===[
  HUD functions
]===]

if minetest.get_modpath("hudbars") ~= nil then
  hb.register_hudbar("sanity", 0xFFFFFF, S("sanity"), { bar = "sanity_hudbars_bar.png", icon = "sanity_eye_closed.png", bgicon = "sanity_eye.png" }, 0, sanity.settings.default_max, false)

  function sanity.hud_update(playername)
    local player = minetest.get_player_by_name(playername)
    if player ~= nil then
      hb.change_hudbar(player, "sanity", sanity.get(playername), sanity.getmax(playername))
    end
  end

  function sanity.hud_remove(playername)
  end

else
  function sanity.sanitystring(playername)
    return S("sanity: @1/@2", sanity.get(playername), sanity.getmax(playername))
  end

  function sanity.hud_add(playername)
    local player = minetest.get_player_by_name(playername)
    local id = player:hud_add({
      hud_elem_type = "text",
      position = { x = 0.5, y=1 },
      text = sanity.sanitystring(playername),
      scale = { x = 0, y = 0 },
      alignment = { x = 1, y = 0},
      direction = 1,
      number = 0xFFFFFF,
      offset = { x = -262, y = -103}
    })
    sanity.playerlist[playername].hudid = id
    return id
  end

  function sanity.hud_update(playername)
    local player = minetest.get_player_by_name(playername)
    player:hud_change(sanity.playerlist[playername].hudid, "text", sanity.sanitystring(playername))

    if sanity > 60 then
    sane = true


  elseif sanity > 40 and sanity <= 60 then
    nervous = true


  elseif sanity > 30 and sanity <= 40 then
    twitchy = true


  elseif sanity > 20 and sanity <= 30 then
    crazy = true


  elseif sanity > 10 and sanity <= 20 then
    cuckoo = true


  elseif sanity > 0 and sanity <= 10 then
    mad = true


  elseif sanity == 0 then
    insane = true
    end
  end

  function sanity.hud_remove(playername)
    local player = minetest.get_player_by_name(playername)
    player:hud_remove(sanity.playerlist[playername].hudid)
  end
end

--[===[
  Helper functions
]===]
sanity.round = function(x)
  return math.ceil(math.floor(x+0.5))
end
